## Uodpornij się na „Fake Newsy”

1. Dzięki grze sprawdzisz swoją odporności na manipulacje w internecie oraz zdobędziesz umiejętności i wiedzę, która pomoże Ci krytycznie analizować treści.
2. Na podstawie 12 przykładów w czterech kategoriach tematycznych ocenisz czy treści z popularnych serwisów internetowych są prawdziwe lub fałszywe.
3. Możesz grać wielokrotnie i sprawdzać swoje postępy!
