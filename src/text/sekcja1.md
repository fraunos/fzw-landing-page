# Nie daj się wkręcić w sieci

Coraz więcej słyszysz o fake newsach, ale nie wiesz jak się przed nimi bronić? Naucz się tego z naszą **edukacyjną grą „Fajnie, że wiesz”**!
