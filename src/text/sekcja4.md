### Więcej o nas
Jeśli chcesz dowiedzieć się o naszych działaniach w ramach edukacji medialnej zapraszamy na stronę Akademii Fact-Checkingu.

Zapraszamy również do podzielenie się opinią o grze „Fajnie, że wiesz” pisząc wiadomość na adres: [kontakt@fajniezewiesz.pl](mailto:kontakt@fajniezewiesz.pl)
